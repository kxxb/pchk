<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$this->title = 'Promo';
?>
<div class="site-index">

    
        <?php
                foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
                echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
                }
             ?>
    

    <div class="body-content">

        <?php $form = ActiveForm::begin(['id' => 'promo-form']); ?>

                    <?= $form->field($model, 'code')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'email') ?>

                    
                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'promo-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

    </div>
</div>
