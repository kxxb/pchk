<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "codes".
 *
 * @property integer $id
 * @property string $code
 * @property integer $staus_id
 * @property string $dt_insert
 * @property string $dt_activation
 *
 * @property UserCodes[] $userCodes
 */
class Codes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'codes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staus_id'], 'integer'],
            [['dt_insert', 'dt_activation'], 'safe'],
            [['code'], 'string', 'max' => 45],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'staus_id' => 'Staus ID',
            'dt_insert' => 'Dt Insert',
            'dt_activation' => 'Dt Activation',
        ];
    }

    
    public static function useCode($code){
        
       $code = Codes::find()->where(['code'=>$code])->one();
       if($code){
        $code->staus_id = 1;
        $code->dt_activation = date("Y-m-d H:i:s");
        $code->update(false); 
       } else {
           return 0;
       }
       return 1;
    }




    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCodes()
    {
        return $this->hasMany(UserCodes::className(), ['code_id' => 'code']);
    }
}
