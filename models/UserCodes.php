<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_codes".
 *
 * @property integer $id
 * @property string $email
 * @property string $code_id
 * @property string $code
 * @property string $dt_reg
 * @property integer $status_id
 *
 * @property Codes $code0
 */
class UserCodes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_codes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dt_reg'], 'safe'],
            ['email', 'email'],
            [['status_id'], 'integer'],
            [['email', 'code_id', 'code'], 'string', 'max' => 45],
            [['code_id'], 'exist', 'skipOnError' => true, 'targetClass' => Codes::className(), 'targetAttribute' => ['code_id' => 'code']],
            [['email'], 'checkEmail'],
            [['code'], 'checkCode'],
        ];
    }
    
    
    public function checkEmail($attribute, $params)
    {
        
        if (!$this->hasErrors()) {
           
            if ( self::checkBot($this->email)) {
          
                
                $this->addError($attribute, 'Ваш email заблокирован на 30 минут. Много неправильных кодов.');
            }
        }
        
    }
    public function checkCode($attribute, $params)
    {
        
        if (!$this->hasErrors()) {
           
            $code = $this->code;
            $email = $this->email;
            
            
                $code = trim(strtoupper($code));
                $codes  = Codes::find()->where(['code'=>$code])->one();

                

                if ($codes){
                    if ($codes->staus_id == 1){
                        self::saveAtepmt($code, $email, $codes->id, 2);
                        $this->addError($attribute, 'Код уже активирован');
                    }
                    
                  } else{
                       
                       $this->addError($attribute, 'Код не найден');
                       self::saveAtepmt($code, $email, null, 3);
                }
        }
        
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'code_id' => 'Code ID',
            'code' => 'Code',
            'dt_reg' => 'Dt Reg',
            'status_id' => 'Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCode0()
    {
        return $this->hasOne(Codes::className(), ['code' => 'code_id']);
    }
    
    
    /* 
     * return 0 - если не бот, 1 - бот
     */
    
    public static function checkBot($email){
        $is_bot = self::find()->where('email = :email and status_id != :status_id and dt_reg >= :dt_reg ', 
                                     [':email'=>$email, 
                                      ':status_id'=>1,
                                      ':dt_reg'=>  date('Y-m-d H:i:s', strtotime('30 minute'))
                                     ])
                                    ->orderBy('dt_reg DESC')->one();
        if ($is_bot)  
          self::saveAtepmt('check-bot', $email, null, 4);
        
        return $is_bot;
        
    }
    
    
            
            
    
    public  function acceptCode(){
        
        if ($this->validate()) {
        
                $code = $this->code;
                $email = $this->email;

                $code = trim(strtoupper($code));
                $codes  = Codes::find()->where(['code'=>$code])->one();




                if ($codes){
                    if ($codes->staus_id == 1){

                        $ret['code'] = Codes::AceptCode($code);
                        $ret['message'] = 'Код  успешно активирован.';

                        self::saveAtepmt($code, $email, $codes->id);




                    } else {
                        $ret['code'] = 2;
                        $ret['message'] = 'Код уже активирован.';

                        self::saveAtepmt($code, $email, $codes->id, 2);
                    }
                } else{
                       $ret['code'] = 3;
                       $ret['message'] = 'Код не найден.';
                       self::saveAtepmt($code, $email, null, 3);
                }


                return $ret;
        
        }
        return null;
        
        
        
    }
    
    
    /* @param status_id  1 - успешно 2 - ошибка код уже активирован 3 - код не найден 4 - пользователь временно заблокирован
     * 
     */
    
    public static function saveAtepmt($code, $email, $code_id = null,  $status_id = 1){
                $userCode = new UserCodes();
                $userCode->code = $code;
                $userCode->code_id  = $code_id;
                $userCode->dt_reg = date("Y-m-d H:i:s"); 
                $userCode->email  = $email;
                $userCode->status_id  = $status_id;
                $userCode->save(false);
    }
    
}
