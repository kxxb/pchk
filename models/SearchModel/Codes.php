<?php

namespace app\models\SearchModel;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Codes as CodesModel;

/**
 * Codes represents the model behind the search form about `\app\models\Codes`.
 */
class Codes extends CodesModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'staus_id'], 'integer'],
            [['code', 'dt_insert', 'dt_activation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CodesModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'staus_id' => $this->staus_id,
            'dt_insert' => $this->dt_insert,
            'dt_activation' => $this->dt_activation,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code]);

        return $dataProvider;
    }
}
